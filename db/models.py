from django.contrib.auth.models import User
from django.db import models


def make_field_upcase(value):
    return value.upper()


# Create your models here.
class Address(models.Model):
    BRAZIL = "BR"
    IRELAND = "IR"
    NETHERLANDS = "NL"
    PORTUGAL = "PT"
    UNITED_KINGDOM = "UK"
    UNITED_STATES = "US"

    COUNTRIES = [
        (BRAZIL, "Brazil"),
        (IRELAND, "Ireland"),
        (NETHERLANDS, "The Netherlands"),
        (PORTUGAL, "Portugal"),
        (UNITED_KINGDOM, "United Kingdom"),
        (UNITED_STATES, "United States"),
    ]

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address1 = models.CharField(max_length=150)
    address2 = models.CharField(max_length=150, default=None, null=True)
    zip_code = models.CharField(max_length=10)
    house_number = models.CharField(max_length=10)
    country_code = models.CharField(max_length=2, choices=COUNTRIES)
    user = models.ForeignKey(
        User, related_name="owner", on_delete=models.CASCADE
    )

    def save(self, *args, **kwargs):
        self.zip_code = self.zip_code.upper()
        self.house_number = self.house_number.upper()
        super().save(*args, **kwargs)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["zip_code", "house_number", "country_code"],
                name="%(app_label)s_%(class)s_unique",
            )
        ]
