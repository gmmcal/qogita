# Image is not usable because app does not expect external connections
# from database, redis or any other dependency at this moment
FROM python:3.10 as base

RUN groupadd -g 1000 uwsgi && \
  useradd -r -m -u 1000 -g uwsgi uwsgi

# USER 0 is the same as USER root
USER 0:1000

WORKDIR /app

COPY --chown=0:1000 --chmod=640 requirements.txt .
COPY --chown=0:1000 --chmod=640 Makefile .
COPY --chown=0:1000 --chmod=640 requirements/ requirements/

RUN make venv

COPY --chown=0:1000 --chmod=640 api/ api/
COPY --chown=0:1000 --chmod=640 db/ db/
COPY --chown=0:1000 --chmod=640 qogita/ qogita/
COPY --chown=0:1000 --chmod=640 tests/ tests/
COPY --chown=0:1000 --chmod=640 manage.py manage.py
COPY --chown=0:1000 --chmod=640 setup.cfg setup.cfg
COPY --chown=0:1000 --chmod=640 pyproject.toml pyproject.toml

RUN chmod 750 $(find /app -type d)

USER root
EXPOSE 8000

ADD entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
