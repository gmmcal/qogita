from django.db.utils import IntegrityError

import pytest

from tests.factories.resources.address import AddressFactory


@pytest.mark.django_db
def test_uniqueness_of_case_insensitive_address():
    AddressFactory(zip_code="1055bm", house_number="1Hs", country_code="NL")
    with pytest.raises(IntegrityError):
        AddressFactory(
            zip_code="1055BM", house_number="1HS", country_code="NL"
        )


@pytest.mark.django_db
def test_same_address_different_countries():
    address1 = AddressFactory(
        zip_code="1055bm", house_number="1Hs", country_code="NL"
    )
    address2 = AddressFactory(
        zip_code="1055BM", house_number="1HS", country_code="PT"
    )
    assert address1.zip_code == address2.zip_code
    assert address1.house_number == address2.house_number
    assert address1.country_code != address2.country_code
