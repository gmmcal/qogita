import pytest

from tests.factories.payloads.address import AddressPayload
from tests.factories.resources.address import AddressFactory


@pytest.fixture
def address_payload():
    return AddressPayload()


@pytest.fixture
def address(user):
    return AddressFactory(user=user)
