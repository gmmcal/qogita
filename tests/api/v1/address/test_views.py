import json

import pytest

from tests.factories.resources.address import AddressFactory
from tests.factories.resources.user import UserFactory


@pytest.mark.django_db
class TestGetAddresses:
    def test_request_no_token(self, api_client):
        response = api_client.post(
            "/v1/address/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "NOT_AUTHENTICATED"

    def test_request_invalid_token(self, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token some_invalid_token")
        response = api_client.post(
            "/v1/address/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "AUTHENTICATION_FAILED"

    def test_pagination(self, token, api_client):
        AddressFactory.create_batch(100, user=token.user)
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.get(
            "/v1/address/",
            data={},
            content_type="application/json",
        )

        assert response.status_code == 200
        assert response.data["count"] == 100
        assert len(response.data["results"]) == 50

    def test_filter_zip_code(self, token, api_client):
        AddressFactory.create_batch(10, user=token.user)
        AddressFactory(zip_code="1055BM", user=token.user)
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.get(
            "/v1/address/?zip_code=1055BM",
            data={},
            content_type="application/json",
        )

        assert response.status_code == 200
        assert len(response.data["results"]) == 1

    def test_filter_partial_name(self, token, api_client):
        AddressFactory.create_batch(3, first_name="Gustavo", user=token.user)
        AddressFactory.create_batch(2, first_name="Rosana", user=token.user)
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.get(
            "/v1/address/?first_name=Gust",
            data={},
            content_type="application/json",
        )

        assert response.status_code == 200
        assert len(response.data["results"]) == 3

    def test_only_returns_user_addresses(self, token, api_client):
        user = UserFactory()
        assert token.user != user
        AddressFactory.create_batch(20, user=token.user)
        AddressFactory.create_batch(30, user=user)
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.get(
            "/v1/address/",
            data={},
            content_type="application/json",
        )

        assert response.status_code == 200
        assert len(response.data["results"]) == 20


@pytest.mark.django_db
class TestCreateAddress:
    def test_request_no_token(self, api_client):
        response = api_client.post(
            "/v1/address/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "NOT_AUTHENTICATED"

    def test_request_invalid_token(self, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token some_invalid_token")
        response = api_client.post(
            "/v1/address/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "AUTHENTICATION_FAILED"

    def test_create_empty_address(self, token, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.post(
            "/v1/address/",
            data={},
            content_type="application/json",
        )

        assert response.status_code == 400
        assert response.data["code"] == "INVALID"

    def test_create_address(self, address_payload, token, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.post(
            "/v1/address/",
            data=json.dumps(address_payload),
            content_type="application/json",
        )

        assert response.status_code == 201


@pytest.mark.django_db
class TestUpdateAddress:
    def test_request_no_token_put(self, address, api_client):
        response = api_client.put(
            f"/v1/address/{address.id}/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "NOT_AUTHENTICATED"

    def test_request_invalid_token_put(self, address, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token some_invalid_token")
        response = api_client.put(
            f"/v1/address/{address.id}/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "AUTHENTICATION_FAILED"

    def test_request_no_token_patch(self, address, api_client):
        response = api_client.patch(
            f"/v1/address/{address.id}/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "NOT_AUTHENTICATED"

    def test_request_invalid_token_patch(self, address, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token some_invalid_token")
        response = api_client.patch(
            f"/v1/address/{address.id}/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "AUTHENTICATION_FAILED"

    def test_update_full_object(self, token, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        address = AddressFactory(user=token.user)
        data = {
            "first_name": "Gustavo",
            "last_name": "Cunha",
            "zip_code": address.zip_code,
            "address1": address.address1,
            "address2": address.address2,
            "house_number": address.house_number,
            "country_code": address.country_code,
        }
        response = api_client.put(
            f"/v1/address/{address.id}/",
            data=json.dumps(data),
            content_type="application/json",
        )
        assert response.status_code == 200
        response_data = response.data
        assert response_data["first_name"] == "Gustavo"
        assert response_data["last_name"] == "Cunha"
        assert response_data["zip_code"] == address.zip_code
        assert response_data["address1"] == address.address1
        assert response_data["address2"] == address.address2
        assert response_data["house_number"] == address.house_number
        assert response_data["country_code"] == address.country_code

    def test_update_partial_object(self, token, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        address = AddressFactory(user=token.user)
        data = {
            "first_name": "Gustavo",
            "last_name": "Cunha",
        }
        response = api_client.patch(
            f"/v1/address/{address.id}/",
            data=json.dumps(data),
            content_type="application/json",
        )
        assert response.status_code == 200
        response_data = response.data
        assert response_data["first_name"] == "Gustavo"
        assert response_data["last_name"] == "Cunha"
        assert response_data["zip_code"] == address.zip_code
        assert response_data["address1"] == address.address1
        assert response_data["address2"] == address.address2
        assert response_data["house_number"] == address.house_number
        assert response_data["country_code"] == address.country_code


@pytest.mark.django_db
class TestDeleteAddress:
    def test_request_no_token(self, api_client):
        response = api_client.post(
            "/v1/address/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "NOT_AUTHENTICATED"

    def test_request_invalid_token(self, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token some_invalid_token")
        response = api_client.post(
            "/v1/address/",
            data={},
            content_type="application/json",
        )
        assert response.status_code == 401
        assert response.data["code"] == "AUTHENTICATION_FAILED"

    def test_delete_own_address(self, address, token, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.delete(
            f"/v1/address/{address.id}/",
            content_type="application/json",
        )

        assert response.status_code == 204

    def test_delete_someone_else_address(self, token, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        address = AddressFactory()
        response = api_client.delete(
            f"/v1/address/{address.id}/",
            content_type="application/json",
        )

        assert response.status_code == 404
        assert response.data["code"] == "NOT_FOUND"
