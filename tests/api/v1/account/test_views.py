import json

import pytest

from tests.factories.payloads.user import UserPayload
from tests.factories.resources.user import UserFactory


@pytest.mark.django_db
class TestAccount:
    def test_successful_create(self, api_client):
        params = UserPayload()
        response = api_client.post(
            "/v1/account/",
            data=json.dumps(params),
            content_type="application/json",
        )

        assert response.status_code == 200
        assert response.data["first_name"] == params["first_name"]
        assert response.data["last_name"] == params["last_name"]
        assert response.data["username"] == params["username"]
        assert response.data["email"] == params["email"]

    def test_duplicated_account(self, api_client):
        params = UserPayload()
        UserFactory(**params)
        response = api_client.post(
            "/v1/account/",
            data=json.dumps(params),
            content_type="application/json",
        )

        assert response.status_code == 500
        assert response.data["code"] == "ERROR"
        assert response.data["message"] == "Account is already existing"
