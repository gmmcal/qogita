import pytest
from rest_framework.test import APIClient

from tests.factories.resources.token import TokenFactory
from tests.factories.resources.user import UserFactory


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def user():
    return UserFactory()


@pytest.fixture
def token(user):
    return TokenFactory(user=user)
