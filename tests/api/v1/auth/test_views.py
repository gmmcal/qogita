import json

import pytest


@pytest.mark.django_db
class TestLogin:
    def test_successful_login(self, user, api_client):
        response = api_client.post(
            "/v1/auth/login/",
            data=json.dumps(
                {"username": user.username, "password": "12345678"}
            ),
            content_type="application/json",
        )

        assert response.status_code == 200

    def test_unsuccessful_login(self, api_client):
        response = api_client.post(
            "/v1/auth/login/",
            data=json.dumps({"username": "username", "password": "12345678"}),
            content_type="application/json",
        )

        assert response.status_code == 400
        assert response.data["code"] == "INVALID"


@pytest.mark.django_db
class TestLogout:
    def test_authenticated_logout(self, token, api_client):
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = api_client.delete(
            "/v1/auth/logout/",
            content_type="application/json",
        )

        assert response.status_code == 204

    def test_unauthenticated_logout(self, api_client):
        response = api_client.delete(
            "/v1/auth/logout/",
            content_type="application/json",
        )

        assert response.status_code == 401
        assert response.data["code"] == "NOT_AUTHENTICATED"
        assert (
            response.data["message"]
            == "Authentication credentials were not provided."
        )
