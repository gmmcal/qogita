import factory
from rest_framework.authtoken.models import Token

from tests.factories.resources.user import UserFactory


class TokenFactory(factory.django.DjangoModelFactory):
    key = factory.Faker("md5")
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Token
