import factory
import factory.fuzzy

from db.models import Address
from tests.factories.resources.user import UserFactory

COUNTRIES = [country[0] for country in Address.COUNTRIES]


class AddressFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    address1 = factory.Faker("street_name")
    zip_code = factory.Faker("postcode")
    house_number = factory.Faker("building_number")
    country_code = factory.fuzzy.FuzzyChoice(COUNTRIES)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Address
