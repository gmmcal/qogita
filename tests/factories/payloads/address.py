import factory
import factory.fuzzy

from db.models import Address

COUNTRIES = [country[0] for country in Address.COUNTRIES]


class AddressPayload(factory.Factory):
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    address1 = factory.Faker("street_name")
    zip_code = factory.Faker("postcode")
    house_number = factory.Faker("building_number")
    country_code = factory.fuzzy.FuzzyChoice(COUNTRIES)

    class Meta:
        model = dict
