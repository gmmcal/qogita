import factory


class UserPayload(factory.Factory):
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")
    username = factory.Faker("user_name")
    password = factory.Faker("password")

    class Meta:
        model = dict
