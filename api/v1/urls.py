from django.conf.urls import include
from django.urls import path

app_name = "base"
application_patterns_v1 = [
    path("auth/", include("api.v1.auth.urls")),
    path("account/", include("api.v1.account.urls")),
    path("address/", include("api.v1.address.urls")),
]
urlpatterns = [
    path("v1/", include((application_patterns_v1, "v1"))),
]
