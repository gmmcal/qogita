from django.contrib.auth.models import User
from django.db.utils import IntegrityError

from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from api.v1.account import serializers


class AccountView(APIView):
    """
    Create account
    """

    input_serializer = serializers.AccountInputSerializer
    output_serializer = serializers.AccountOutputSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.input_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user = User.objects.create(**serializer.validated_data)
        except IntegrityError:
            raise APIException(
                "Account is already existing", code=HTTP_400_BAD_REQUEST
            )

        output = self.output_serializer(user.__dict__)
        return Response(data=output.data)
