from django.urls import path

from api.v1.account import views

app_name = "auth"
urlpatterns = [
    path("", views.AccountView.as_view()),
]
