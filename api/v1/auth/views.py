from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.views import APIView

from api.v1.auth import serializers


class LoginView(ObtainAuthToken):
    """
    Get auth token
    """

    output_serializer = serializers.LoginOutputSerializer


class LogoutView(APIView):
    """
    Logout user
    """

    authentication_classes = [
        TokenAuthentication,
    ]
    permission_classes = [IsAuthenticated]

    def delete(self, request, *args, **kwargs):
        token = Token.objects.get(user=request.user)
        token.delete()
        return Response(status=HTTP_204_NO_CONTENT)
