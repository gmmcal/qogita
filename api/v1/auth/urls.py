from django.urls import path

from api.v1.auth import views

app_name = "auth"
urlpatterns = [
    path("login/", views.LoginView.as_view()),
    path("logout/", views.LogoutView.as_view()),
]
