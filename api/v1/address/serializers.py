from rest_framework import serializers

from db.models import Address


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = [
            "id",
            "first_name",
            "last_name",
            "address1",
            "address2",
            "zip_code",
            "house_number",
            "country_code",
        ]
