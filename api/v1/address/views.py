from django_filters import rest_framework as filters
from rest_framework import mixins, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from api.v1.address import serializers
from db.models import Address


class ProductFilter(filters.FilterSet):
    first_name = filters.CharFilter(lookup_expr="icontains")
    last_name = filters.CharFilter(lookup_expr="icontains")
    address1 = filters.CharFilter(lookup_expr="icontains")
    zip_code = filters.CharFilter(lookup_expr="icontains")
    country_code = filters.CharFilter(lookup_expr="iexact")

    class Meta:
        model = Address
        fields = ["first_name", "last_name"]


class AddressView(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    authentication_classes = [
        TokenAuthentication,
    ]
    permission_classes = [IsAuthenticated]

    serializer_class = serializers.AddressSerializer
    filterset_class = ProductFilter

    def get_queryset(self):
        return Address.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
