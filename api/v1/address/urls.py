from rest_framework.routers import DefaultRouter

from api.v1.address import views

router = DefaultRouter()
router.register("", views.AddressView, basename="address")
urlpatterns = router.urls
