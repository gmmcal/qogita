from django.http import Http404

from rest_framework.exceptions import APIException, NotFound
from rest_framework.response import Response


def exception_handler(exc, context=None):
    if isinstance(exc, Http404):
        exc = NotFound()

    code = getattr(exc, "code", "")
    message = str(exc)
    errors = getattr(exc, "errors", [])

    if not code and isinstance(exc, APIException):
        code = getattr(exc, "default_code", "FAILED")
        code = code.upper()

    data = dict(
        message=message,
        errors=errors,
        code=code,
    )

    return Response(data, status=exc.status_code)
