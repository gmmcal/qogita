from drf_yasg.inspectors import SwaggerAutoSchema
from rest_framework import serializers, status


class AutoSchema(SwaggerAutoSchema):
    def get_request_serializer(self):
        kls = self._get_serializer(self.view, "input", self.method.lower())
        if kls:
            return kls()
        return super().get_request_serializer()

    def get_response_serializers(self):
        kls = self._get_serializer(self.view, "output", self.method.lower())
        status_code = self._get_status_code(self.view, self.method.lower())
        if kls:
            return {status_code: kls and kls()}
        return super().get_response_serializers()

    @staticmethod
    def _get_serializer(view, target, method):
        possibilities = [
            "{}_{}_serializer".format(method, target),
            "{}_serializer".format(target),
        ]

        for attr in possibilities:
            serializer = getattr(view, attr, None)
            if serializer and isinstance(
                serializer, serializers.SerializerMetaclass
            ):
                return serializer

    @staticmethod
    def _get_status_code(view, method):
        status_message_response = getattr(
            view, "{}_status_code".format(method), None
        )
        if status_message_response:
            return status_message_response

        return status.HTTP_200_OK
