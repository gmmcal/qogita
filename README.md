# Qogita Test

## Task - Address Book API
Your task is to design a REST-ful API and create a server for it in Python, using the Django Rest Framework, which provides functionality to authenticate a user and to manage the addresses associated with their account. Only authenticated users should be able to view and manipulate addresses.
Assume this API will be used by a variety of clients, like public and internal websites, or native mobile apps.

You should document the API so it is clear what endpoints are available, how they can be queried and what response can be expected in return.
The task should take about 3-4 hours to complete, and you will be given a week to do this. You
should commit this code to a code repository of your choice and share this with us.

Requirements
1. User is able to create a new address
    * *User will not be able to add a duplicated address to their account
1. User is able to retrieve all their postal addresses
    * *User is able to retrieve a large number of address entries in a practical way
    * *User is able to filter retrieved addresses using request parameters
1. User is able to update existing addresses
1. User is able to delete one address
    * *User is able to delete multiple addresses
    * *User is able to authenticate with a username and a password
1. *User can log out

*Bonus requirements - feel free to give any of these a go if you have any spare time, but we don’t expect you to complete them all.

### Additional Notes
* A user can have a large number of addresses;
* A user can have addresses from any country;
* A user's client can hold state (if necessary for certain endpoints);
* You must ensure the application can be run by another engineer with access to the repository;
* You do not need to deploy this code to a server;
* You should document any assumptions you make about the product, especially where you would have a question about it;

## Solution

The solution is built in `Django` and `Django Rest Framework`, using the simplest setup possible. Tests are executed with `pytest` and all import commands are on `Makefile`.

To run the application locally, you need to run a few key commands.
* `cp .env.example .env`
* `make clean venv`
* `make test (to run tests)`
* `make server (to run server)`

Server is exposed on port 8000 by default. You can access on your browser via `localhost:8000`.

To be sure everything will run fine, you need to manually create a Postgres database. The default name is `qogita`. This can be changed on your `.env` file.

### Docker

A Docker solution is also available using docker compose. To start server, simply run `docker-compose up` and you should be able to access server via `localhost:8000`

### Docs

Docs are automatically generated based on code. You can view a swagger version of docs by accessing `localhost:8000/docs`.

### Architecture

As mentioned before, I decided to go with the simplest code possible, so some business logic is handled on `View` level or by a `ViewSet`. This decision was made because app is very simple. On a mode complex application, probably all logic currently on view would be moved to a `domains` application and views would call those objects.

### Missing feature

The feature `User is able to delete multiple addresses` was not implemented. Everything else should be present.
