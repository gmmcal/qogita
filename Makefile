.PHONY: clean
clean:
	@find . -name *.pyc -delete
	@rm -rf *.egg-info build
	@rm -rf coverage.xml .coverage
	@rm -rf venv

venv: venv/.venv_test

venv/.venv_base:
	@python3.10 -m venv venv
	@touch $@

venv/.venv_prod: venv/.venv_base
	@venv/bin/pip install -r requirements.txt
	@touch $@

venv/.venv_test: venv/.venv_prod requirements/test.txt
	@venv/bin/pip install -r requirements/test.txt
	@touch $@

requirements.txt: | venv/.venv_base
	@venv/bin/pip install pip-tools
	@venv/bin/pip-compile \
		--no-emit-index-url --no-emit-trusted-host --upgrade \
		--output-file requirements.txt \
		--build-isolation \
		requirements/app.txt
	@echo "Successfully Updated requirements"

.PHONY: test
test: venv/.venv_test lint format
	@venv/bin/py.test tests --cov=. --junitxml=./junit.xml
	@venv/bin/coverage xml -i -o ./coverage.xml

.PHONY: migrate
migrate:
	@venv/bin/python manage.py migrate --noinput

# LINT

.PHONY: lint
lint: lint/flake8 lint/mypy

.PHONY: lint/flake8
lint/flake8: venv/.venv_test
	@venv/bin/flake8 .

.PHONY: lint/mypy
lint/mypy: venv/.venv_test
	@venv/bin/mypy .

.PHONY: format
format: format/isort format/black

.PHONY: format/isort
format/isort: venv/.venv_test
	@venv/bin/isort .

.PHONY: format/black
format/black: venv/.venv_test
	@venv/bin/black --verbose .

.PHONY: server
server:
	@venv/bin/python manage.py runserver 0.0.0.0:8000
